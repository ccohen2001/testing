my_list = ["s", "a", "j", "q", "l", "m", "v", "l", "t", "h", "b", "w",
"r", "g", "n", "c", "y", "i", "z", "a", "l", "t", "x", "e",
"k","o", "r", "u", "p", "l", "n", "c", "d", "q", "l", "w"]

num_of_letters = {}

for letters in my_list:
    if letters in num_of_letters:
        num_of_letters[letters] = num_of_letters[letters] + 1
    else:
        num_of_letters[letters] = 1

print(num_of_letters)
